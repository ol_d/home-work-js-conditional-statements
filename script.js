// // #1
var a = parseInt(prompt('a = ?'));
var b = parseInt(prompt('b = ?'));

var c = (a + b < 4) ? 'Malo' : 'Mnogo';

document.write(c + '<br><hr>');

// // #2
var login = prompt("Login");
var message;

var ansv = login == 'Vasia' ? 'Privet' : login == 'Director' ? 'Zdravstvuite' : login == '' ? 'Net logina' : '';

document.write(ansv + '<br><hr>');

// // #3
var num1 = parseInt(prompt('num 1'));
var num2 = parseInt(prompt('num 2'));
var counter = 0;
for (var i = num1; i <= num2; i++) {
    if(i < 0){
        document.write(i+ '<br>');
    }
    counter += i;
}

document.write('Sum: ' + counter + '<br><hr>');


// #4_1
var width1 = parseInt(prompt('rectangle width:'));
var height1 = parseInt(prompt('rectangle height:'));

for(var i = 1; i <= height1; i++) {
    for(var j = 1; j<= width1; j++){
        document.write('*');
    }
    document.write('<br>');
}

document.write('<hr>');

// #4_2
var height2 = parseInt(prompt('triangle height:'));

for(var i = 0; i < height2; i++) {
    for(var j = 2; j <= height2-i; j++){
        document.write('_');
    }
    for(var j = 0; j < i + i + 1; j++){
        document.write('*');
    }
    document.write('<br>')
}

document.write('<hr>');

// #4_3
var height3 = parseInt(prompt('triangle 2 height:')); 

var counterHeight = 0;

while(counterHeight < height3){
    counterHeight++;
    var i = 0;
    while(i < counterHeight){
        document.write('*');
        i++;
    }
    document.write('<br>');
}

document.write('<hr>');

// #4_4
var height4 = parseInt(prompt('Enter "5" "9" or "17":'));

if(height4 != 5 && height4 != 9 && height4 != 17) {
    document.write("If you don't want to do it, fine. Have a nice day.");
} else {
    for(var i = 0; i < (height4 - 1) / 2; i++) {
        for(var j = 1; j <= (height4 - 1)/2-i; j++){
            document.write('_');
        }
        for(var j = 0; j < i + i + 1; j++){
            document.write('*');
        }
        document.write('<br>')
    }   
    var counterWidth = 0;
    while(counterWidth < height4){
        document.write('*');
        counterWidth++;
    }
    document.write('<br>')
    for(var i = (height4 - 1)/2; i > 0; i--){
        for(var j = 0; j <= (height4 - 1)/2-i; j++){
            document.write('_');
        }
        for(var j = 0; j < i + i - 1; j++){
            document.write('*');
        }
        document.write('<br>')
    }
}

